import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import '@/axios'

// Vue Notification
import Notification from '@kyvg/vue3-notification';

// Font Awesome for Vue JS
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

// Importing FontAwesome library
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons"

library.add(fas)

require('@/store/subscriber')

store.dispatch('auth/attempt', localStorage.getItem('token')).then(() => {
    createApp(App)
        .use(store)
        .use(router)
        .use(Notification)
        .component('fa', FontAwesomeIcon)
        .mount('#app')
})

