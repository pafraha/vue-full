import axios from "axios";

// axios.defaults.baseURL = 'https://rocky-river-17027.herokuapp.com';
axios.defaults.baseURL = 'http://127.0.0.1:8001';
axios.defaults.withCredentials = true;
