import { createRouter, createWebHistory } from 'vue-router'
// Components
import Home from '../components/Home.vue'
import Login from '../components/Login.vue'
import Register from '../components/Register.vue'
import Forgot from '../components/Forgot.vue'
import Reset from '../components/Reset.vue'
import Change from '../components/Dashbord'
import store from '@/store';

const routes = [
    { path: '/', component: Home },
    { path: '/login', component: Login, beforeEnter: (to, from, next) => {
        if (store.getters['auth/authenticated']){
            return next({
                path: '/change'
            })
        }
        next()
        }},
    { path: '/register', component: Register },
    { path: '/forgot', component: Forgot, beforeEnter: (to, from, next) => {
            if (store.getters['auth/authenticated']){
                return next({
                    path: '/change'
                })
            }
            next()
        }},
    { path: '/change', component: Change, beforeEnter: (to, from, next) => {
        if (!store.getters['auth/authenticated']){
            return next({
                path: '/login'
            })
        }
        next()
        }},
    { path: '/reset/:token', component: Reset, beforeEnter: (to, from, next) => {
        if (store.getters['auth/authenticated']){
            return next({
                path: '/change'
            })
        }
        next()
        }},
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
